import request from '@/utils/request'

export function logout() {
  return request({
    url: '/user/logout',
    method: 'get'
  })
}

export function getByDoctorId(data) {
  return request({
    url: `/sysUserMain/getByDoctorId/${data}`,
    method: 'post'
  })
}

export function deleteDoctorById(data) {
  return request({
    url: `/sysUserMain/deleteDoctorById/${data}`,
    method: 'post'
  })
}

export function updateDoctor(data) {
  return request({
    url: '/sysUserMain/updateDoctor',
    method: 'put',
    data
  })
}

export function getAllDoctor(data) {
  return request({
    url: '/sysUserMain/getAllDoctor',
    method: 'get',
    data
  })
}

export function getByUserId(data) {
  return request({
    url: `/sysUserMain/getByUserId/${data}`,
    method: 'post'
  })
}

export function deleteUserById(data) {
  return request({
    url: `/sysUserMain/deleteUserById/${data}`,
    method: 'post'
  })
}

export function updateUser(data) {
  return request({
    url: '/sysUserMain/updateUser',
    method: 'put',
    data
  })
}

export function getAllUser(data) {
  return request({
    url: '/sysUserMain/getAllUser',
    method: 'get',
    data
  })
}

export function updateOrder(data) {
  return request({
    url: '/sysUserMain/updateOrder',
    method: 'put',
    data
  })
}

export function getAllOrder(data) {
  return request({
    url: '/sysUserMain/getAllOrder',
    method: 'get',
    data
  })
}

export function login(data) {
  return request({
    url: '/sysUser/login',
    method: 'post',
    data
  })
}

export function getAllDoctorSh(data) {
  return request({
    url: '/sysUserMain/getAllDoctorSh',
    method: 'get',
    data
  })
}

export function agreeDoctorSh(data) {
  return request({
    url: '/sysUserMain/agreeDoctorSh',
    method: 'put',
    data
  })
}

export function refuseDoctorSh(data) {
  return request({
    url: `/sysUserMain/refuseDoctorSh/${data}`,
    method: 'post'
  })
}
