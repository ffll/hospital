import request from '@/utils/request'

export function getLiuYanJL(data) {
  return request({
    url: `/comments/getLiuYanJL/${data}`,
    method: 'post'
  })
}

export function saveComments(data) {
  return request({
    url: `/comments/saveComments`,
    method: 'post',
    data
  })
}

export function saveScore(data) {
  return request({
    url: `/userMain/saveScore`,
    method: 'post',
    data
  })
}

export function updateOrder(data) {
  return request({
    url: '/userMain/updateOrder',
    method: 'put',
    data
  })
}

export function getDoctorByName(data) {
  return request({
    url: `/userMain/getDoctorByName/${data}`,
    method: 'post'
  })
}

export function getAllDoctor() {
  return request({
    url: '/userMain/getAllDoctor',
    method: 'get'
  })
}

export function deleteOrderById(data) {
  return request({
    url: `/userMain/deleteOrderById/${data}`,
    method: 'post'
  })
}

export function getByOrderId(data) {
  return request({
    url: `/userMain/getByOrderId/${data}`,
    method: 'post'
  })
}

export function getUserOrder(data) {
  return request({
    url: `/userMain/getUserOrder/${data}`,
    method: 'post'
  })
}

export function saveOrder(data) {
  return request({
    url: '/userMain/saveOrder',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'get'
  })
}

export function getUser(data) {
  return request({
    url: '/user/info',
    method: 'get',
    data
  })
}

export function getDoctor(data) {
  return request({
    url: `/userMain/getDoctor/${data}`,
    method: 'post'
  })
}

export function register(data) {
  return request({
    url: '/user/register',
    method: 'post',
    data
  })
}

export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}

export function getInfo() {
  return request({
    url: '/sysUser/info',
    method: 'get'
  })
}

export function update(data) {
  return request({
    url: '/sysUser/update',
    method: 'put',
    data
  })
}
export function updatePwd(data) {
  return request({
    url: '/user/updatePwd',
    method: 'put',
    data
  })
}
export function updateInfo(data) {
  return request({
    url: '/user/updateInfo',
    method: 'put',
    data
  })
}
export function getSysUser(data) {
  return request({
    url: '/sysUser/getSysUser',
    method: 'get',
    data
  })
}
