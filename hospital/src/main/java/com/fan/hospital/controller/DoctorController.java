package com.fan.hospital.controller;

import com.fan.hospital.enums.ResultEnum;
import com.fan.hospital.pojo.Doctor;
import com.fan.hospital.pojo.User;
import com.fan.hospital.pojo.vo.TokenVo;
import com.fan.hospital.enums.UserTypeEnum;
import com.fan.hospital.service.DoctorService;
import com.fan.hospital.service.UserService;
import com.fan.hospital.shiro.LoginToken;
import com.fan.hospital.shiro.LoginUser;
import com.fan.hospital.utils.Result;
import com.fan.hospital.utils.ShiroUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

@RestController
@RequestMapping("/doctor")
public class DoctorController {
    @Autowired
    private DoctorService doctorService;
    /**
     * 注册
     *
     * @param doctor
     * @return
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public Result<?> register(@RequestBody Doctor doctor) {
        doctorService.save(doctor);
        return new Result<>("请等待管理员审核");
    }
    /**
     * 登录
     *
     * @param doctor
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Result<TokenVo> login(@RequestBody Doctor doctor) {
        Subject subject = SecurityUtils.getSubject();
        AuthenticationToken authenticationToken =
                new LoginToken(doctor.getUserName(), doctor.getPassWord(), UserTypeEnum.DOCTOR);
        try {
            subject.login(authenticationToken);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result<>(ResultEnum.LOGIN_PARAM_ERROR);
        }
        Serializable token = subject.getSession().getId();
        return new Result<>(new TokenVo(token));
    }

}
