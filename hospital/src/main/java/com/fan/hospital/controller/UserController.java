package com.fan.hospital.controller;

import com.fan.hospital.enums.ResultEnum;
import com.fan.hospital.pojo.SysUser;
import com.fan.hospital.pojo.User;
import com.fan.hospital.pojo.vo.TokenVo;
import com.fan.hospital.enums.UserTypeEnum;
import com.fan.hospital.service.UserService;
import com.fan.hospital.shiro.LoginToken;
import com.fan.hospital.shiro.LoginUser;
import com.fan.hospital.utils.Result;
import com.fan.hospital.utils.ShiroUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    /**
     * 注册
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public Result<?> register(@RequestBody User user) {
        userService.save(user);
        return new Result<>("注册成功");
    }
    /**
     * 登录
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Result<TokenVo> login(@RequestBody User user) {
        Subject subject = SecurityUtils.getSubject();
        AuthenticationToken authenticationToken =
                new LoginToken(user.getUserName(), user.getPassWord(), UserTypeEnum.USER);
        try {
            subject.login(authenticationToken);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result<>(ResultEnum.LOGIN_PARAM_ERROR);
        }
        Serializable token = subject.getSession().getId();
        return new Result<>(new TokenVo(token));
    }

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public Result<LoginUser> info() {
        // LoginUser user = ShiroUtils.getLoginUser();
        /*Subject user = SecurityUtils.getSubject();
        System.out.println(user);
        String logcode =user.getPrincipal().toString();
        System.out.println(logcode);
        LoginUser loginUser = new LoginUser() ;
        BeanUtils.copyProperties(user, loginUser);*/
        LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        return new Result<>(loginUser);
    }


    /**
     * 退出登录
     *
     * @return
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public Result<?> logout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return new Result<>("退出成功！");
    }
    /**
     * 修改密码
     * @param user
     * @return
     */
    @RequestMapping(value = "/updatePwd", method = RequestMethod.PUT)
    public Result<?> updatePwd(@RequestBody User user) {
        userService.updatePwd(user);    
        return new Result<>("修改成功");
    }

    /**
     * 修改当前登录用户
     * @param user
     * @return
     */
    @RequestMapping(value = "/updateInfo", method = RequestMethod.PUT)
    public Result<?> updateInfo(@RequestBody User user) {
        userService.update(user);
        // 更新当前登录用户
        LoginUser loginUser = new LoginUser();
        BeanUtils.copyProperties(user, loginUser);
        loginUser.setUserId(user.getUserId());
        ShiroUtils.setUser(loginUser);
        return new Result<>("修改成功！");
    }


}
