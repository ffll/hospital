package com.fan.hospital.enums;

import lombok.Getter;

@Getter
public enum UserTypeEnum {
    USER(0,"普通用户"),
    SYS_USER(1,"管理员"),
    DOCTOR(2,"医生")
    ;
    private Integer code;
    private String msg;

    UserTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
