package com.fan.hospital.exception;

import com.fan.hospital.enums.ResultEnum;

/**
 * 自定义的异常
 * @author ：naruto
 * @version 1.0
 * @date ：Created in 2021/7/19 15:28
 * @modified By：
 */
public class HospitalException extends RuntimeException {

    private static final long serialVersionUID = 2450214686001409867L;

    private Integer errorCode = ResultEnum.ERROR.getCode();

    public HospitalException(ResultEnum resultEnum) {
        super(resultEnum.getMsg());
        this.errorCode = resultEnum.getCode();
    }

    public HospitalException(ResultEnum resultEnum, Throwable throwable) {
        super(resultEnum.getMsg(), throwable);
        this.errorCode = resultEnum.getCode();
    }

    public HospitalException(Integer errorCode, String msg) {
        super(msg);
        this.errorCode = errorCode;
    }

    public HospitalException(String msg) {
        super(msg);
    }

    public HospitalException(Throwable throwable) {
        super(throwable);
    }

    public HospitalException(String msg, Throwable throwable) {
        super(msg, throwable);
    }

    public Integer getErrorCode() {
        return errorCode;
    }
}
