package com.fan.hospital.mapper;

import com.fan.hospital.pojo.Comments;
import com.fan.hospital.pojo.vo.CommentsVo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CommentsMapper {
    void saveComments(Comments comments);
    List<CommentsVo> getLiuYanJL(long userId);
}
