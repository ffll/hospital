package com.fan.hospital.mapper;

import com.fan.hospital.pojo.Doctor;
import com.fan.hospital.pojo.User;
import org.springframework.stereotype.Component;

@Component
public interface DoctorMapper {
    /**
     * 保存
     * @param doctor
     */
    void save(Doctor doctor);
    /**
     * 根据用户名查询
     * @param username
     * @return
     */
    Doctor getByUsername(String username);
    /**
     * 修改密码
     * @param doctor
     */
    void updatePwd(Doctor doctor);

    /**
     * 修改用户
     * @param doctor
     */
    void update(Doctor doctor);


}
