package com.fan.hospital.mapper;

import com.fan.hospital.pojo.Doctor;
import com.fan.hospital.pojo.DoctorSh;
import com.fan.hospital.pojo.User;
import com.fan.hospital.pojo.vo.OrderVo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysUserMainMapper {
    List<User> getAllUser();
    List<Doctor> getAllDoctor();
    List<OrderVo> getAllOrder();
    List<User> getUserByName(String username);
    void updateUser(User user);
    void updateOrder(OrderVo orderVo);
    List<User> getByUserId(long userId);
    void deleteUserById(long userId);
    void updateDoctor(Doctor doctor);
    void deleteDoctorById(long doctorId);
    List<DoctorSh> getAllDoctorSh();
    void agreeDoctorSh(DoctorSh doctorSh);
    void saveDoctorSh(DoctorSh doctorSh);
    void refuseDoctorSh(long doctorId);
}
