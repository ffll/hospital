package com.fan.hospital.mapper;

import com.fan.hospital.pojo.Doctor;
import com.fan.hospital.pojo.Order;
import com.fan.hospital.pojo.Score;
import com.fan.hospital.pojo.dto.OrderDto;
import com.fan.hospital.pojo.vo.OrderVo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserMainMapper {
    List<Doctor> getDoctor(String dept);
    void saveOrder(Order order);
    Doctor getDoctorByDoctorId(String doctorName);
    List<OrderVo> getUserOrder(long userId);
    void deleteOrderById(long orderId);
    List<Doctor> getAllDoctor();
    List<Doctor> getDoctorByName(String username);
    void updateOrder(OrderVo orderVo);
    void saveScore(Score score);
}
