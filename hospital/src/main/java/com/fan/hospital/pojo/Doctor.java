package com.fan.hospital.pojo;

import com.fan.hospital.shiro.LoginUser;
import lombok.Data;

import java.io.Serializable;

@Data
public class Doctor extends LoginUser implements Serializable {
    private Long doctorId ;

    private String userName;

    private String passWord;

    private String sex;

    private int age;

    private String dept;

    //1.住院医师;2.主治医师;3.副主任医师;4.主任医师
    private String doctorGrade;

    private int score;

    private long phone;
}
