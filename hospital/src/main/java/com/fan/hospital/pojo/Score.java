package com.fan.hospital.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class Score implements Serializable {
    private String doctorId;
    private long score;
}
