package com.fan.hospital.pojo;

import com.fan.hospital.shiro.LoginUser;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ：naruto
 * @version 1.0
 * @date ：Created in 2021/7/22 9:44
 * @modified By：
 */
@Data
public class SysUser extends LoginUser implements Serializable {

    private Long adminId ;

    private String passWord;

    private String userName;


}
