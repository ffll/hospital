package com.fan.hospital.pojo;

import com.fan.hospital.shiro.LoginUser;
import lombok.Data;
import java.io.Serializable;

@Data
public class User extends LoginUser implements Serializable {
    private Long userId ;

    private String userName;

    private String passWord;

    private String sex;

    private int age;

    private long phone;

    private Integer deleted;

}
