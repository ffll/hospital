package com.fan.hospital.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
//修改密码实体类
public class UpdatePwdDto {
    private long id;
    private String CurrentPwd;//旧密码
    private  String newPwd;//新密码
}
