package com.fan.hospital.service;

import com.fan.hospital.pojo.Comments;
import com.fan.hospital.pojo.vo.CommentsVo;

import java.util.List;

public interface CommentsService {
    /**
     * 保存
     * @param comments
     */
    void saveComments(Comments comments);
    List<CommentsVo> getLiuYanJL(long userId);
}
