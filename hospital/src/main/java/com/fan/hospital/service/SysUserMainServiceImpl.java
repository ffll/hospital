package com.fan.hospital.service;

import com.fan.hospital.mapper.SysUserMainMapper;
import com.fan.hospital.pojo.Doctor;
import com.fan.hospital.pojo.DoctorSh;
import com.fan.hospital.pojo.User;
import com.fan.hospital.pojo.vo.OrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class SysUserMainServiceImpl implements SysUserMainService{
    @Autowired
    private SysUserMainMapper sysUserMainMapper;
    @Override
    public List<User> getAllUser() {
        return sysUserMainMapper.getAllUser();
    }

    @Override
    public List<Doctor> getAllDoctor() {
        return sysUserMainMapper.getAllDoctor();
    }

    @Override
    public List<OrderVo> getAllOrder() {
        return sysUserMainMapper.getAllOrder();
    }

    @Override
    public List<User> getUserByName(String username) {
        return sysUserMainMapper.getUserByName(username);
    }

    @Override
    public void updateUser(User user) {
        sysUserMainMapper.updateUser(user);
    }

    @Override
    public void updateOrder(OrderVo orderVo) {
        sysUserMainMapper.updateOrder(orderVo);
    }

    @Override
    public List<User> getByUserId(long userId) {
        return sysUserMainMapper.getByUserId(userId);
    }

    @Override
    public void deleteUserById(long userId) {
        sysUserMainMapper.deleteUserById(userId);
    }

    @Override
    public void updateDoctor(Doctor doctor) {
        sysUserMainMapper.updateDoctor(doctor);
    }

    @Override
    public void deleteDoctorById(long doctorId) {
        sysUserMainMapper.deleteDoctorById(doctorId);
    }

    @Override
    public List<DoctorSh> getAllDoctorSh() {
        return sysUserMainMapper.getAllDoctorSh();
    }

    @Override
    public void agreeDoctorSh(DoctorSh doctorSh) {
        sysUserMainMapper.agreeDoctorSh(doctorSh);
        sysUserMainMapper.saveDoctorSh(doctorSh);
    }

    @Override
    public void refuseDoctorSh(long doctorId) {
        sysUserMainMapper.refuseDoctorSh(doctorId);
        sysUserMainMapper.deleteDoctorById(doctorId);
    }
}
