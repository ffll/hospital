package com.fan.hospital.service;

import com.fan.hospital.pojo.SysUser;
import com.fan.hospital.pojo.dto.UpdatePwdDto;

/**
 * 系统用户
 */
public interface SysUserService {


    /**
     * 通过用户名查询用户
     * @param username
     * @return
     */
    SysUser querySysUserByName(String username);
    /**
     * 修改用户
     * @param sysUser
     */
    void update(SysUser sysUser);

    /**
     * 修改密码
     * @param updatePwdDto
     */
    void updatePwd(UpdatePwdDto updatePwdDto);

    /**
     * 获取管理员信息
     * @return
     */
    SysUser getSysUser();



}
