package com.fan.hospital.service;

import com.fan.hospital.pojo.Doctor;
import com.fan.hospital.pojo.Order;
import com.fan.hospital.pojo.Score;
import com.fan.hospital.pojo.User;
import com.fan.hospital.pojo.dto.OrderDto;
import com.fan.hospital.pojo.vo.OrderVo;

import java.util.List;

public interface UserMainService {
    /**
     * 根据部门查医生
     */
    List<Doctor> getDoctor(String dept);
    /**
     * 保存预约信息
     */
    void saveOrder(OrderDto orderDto);
    /**
     * 根据Id获取医生信息
     */
    Doctor getDoctorByDoctorId(String doctorName);
    /**
     * 根据userId获取订单信息
     */
    List<OrderVo> getUserOrder(long userId);

    void deleteOrderById(long orderId);

    List<Doctor> getAllDoctor();
    List<Doctor> getDoctorByName(String username);
    void updateOrder(OrderVo orderVo);
    void saveScore(Score score);
}
