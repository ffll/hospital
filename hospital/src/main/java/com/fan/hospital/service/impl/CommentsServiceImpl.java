package com.fan.hospital.service.impl;

import com.fan.hospital.mapper.CommentsMapper;
import com.fan.hospital.pojo.Comments;
import com.fan.hospital.pojo.vo.CommentsVo;
import com.fan.hospital.service.CommentsService;
import com.fan.hospital.utils.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CommentsServiceImpl implements CommentsService {
    @Autowired
    private CommentsMapper commentsMapper;
    @Autowired
    private IdWorker idWorker;
    @Override
    public void saveComments(Comments comments) {
        long id = idWorker.nextId();
        comments.setId(id);
        comments.setTime(new Date());
        commentsMapper.saveComments(comments);
    }

    @Override
    public List<CommentsVo> getLiuYanJL(long userId) {
        return commentsMapper.getLiuYanJL(userId);
    }
}
