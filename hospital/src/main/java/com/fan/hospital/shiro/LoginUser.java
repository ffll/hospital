package com.fan.hospital.shiro;

import com.fan.hospital.utils.IdWorker;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

@Data
public class LoginUser implements Serializable {

    private Long adminId;
    private Long doctorId ;
    private Long userId;
    private String adminName;
    private Long id;

    private int age;

    private long phone;


    private String userName;
    private String sex;

    private String dept;

    //1.住院医师;2.主治医师;3.副主任医师;4.主任医师
    private String doctorGrade;

    private int score;



    private String passWord;


    private Integer deleted;


}
