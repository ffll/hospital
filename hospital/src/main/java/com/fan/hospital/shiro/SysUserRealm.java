package com.fan.hospital.shiro;

import com.fan.hospital.enums.ResultEnum;
import com.fan.hospital.enums.UserTypeEnum;
import com.fan.hospital.exception.HospitalException;
import com.fan.hospital.pojo.Doctor;
import com.fan.hospital.pojo.SysUser;
import com.fan.hospital.pojo.User;
import com.fan.hospital.service.DoctorService;
import com.fan.hospital.service.SysUserService;
import com.fan.hospital.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 核心配置类：
 * Shiro：认证/授权
 * @author ：naruto
 * @version 1.0
 * @date ：Created in 2021/7/23 10:11
 * @modified By：
 */
@Component("sysUserRealm")
public class SysUserRealm extends AuthorizingRealm {

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private UserService userService;
    @Autowired
    private DoctorService doctorService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return new SimpleAuthorizationInfo();
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        LoginToken loginToken = (LoginToken) authenticationToken;
        String username = loginToken.getUsername();
        loginToken.setRememberMe(true);
        if (loginToken.getUserType() == UserTypeEnum.SYS_USER) {
            // 管理员
            SysUser sysUser = sysUserService.querySysUserByName(username);
            if (sysUser == null) {
                throw new HospitalException(ResultEnum.LOGIN_PARAM_ERROR);
            }
            // 转json，或者拷贝属性
            LoginUser loginUser = new LoginUser();
            BeanUtils.copyProperties(sysUser, loginUser);
            return new SimpleAuthenticationInfo(loginUser, sysUser.getPassWord(), this.getName());
        } else if(loginToken.getUserType() == UserTypeEnum.USER){
            // 普通用户
            User user = userService.getByUsername(username);
            if (user == null) {
                throw new HospitalException(ResultEnum.LOGIN_PARAM_ERROR);
            }
            LoginUser loginUser = new LoginUser();
            BeanUtils.copyProperties(user, loginUser);
            return new SimpleAuthenticationInfo(loginUser,loginUser.getPassWord(), this.getName());
        }else {
            // 医生
            Doctor doctor = doctorService.getByUsername(username);
            if (doctor == null) {
                throw new HospitalException(ResultEnum.LOGIN_PARAM_ERROR);
            }
            LoginUser loginUser = new LoginUser();
            BeanUtils.copyProperties(doctor, loginUser);
            return new SimpleAuthenticationInfo(loginUser, doctor.getPassWord(), this.getName());
        }
    }
}
